﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Video;
using AForge.Video.DirectShow;

namespace CarPickupLine
{
    public partial class CamLoad : Form
    {
        public CamLoad()
        {

            InitializeComponent();
        }


        //Variables for the webcam
        private FilterInfoCollection webcam;
        private VideoCaptureDevice cam;
        public int wait { get; set; }
        public string current { get; set; }
        private void CamLoad_Load(object sender, EventArgs e)
        {
            //Create a new webcam 
            webcam = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            foreach (FilterInfo VideoCaptureDevice in webcam)
            {
                //Adds computer webcam to the combobox to allow selection
                comboBox1.Items.Add(VideoCaptureDevice.Name);
            }
            comboBox1.SelectedIndex = 0;
            //Selects the currently selected webcam source and creates a new instance of cam from it
            cam = new VideoCaptureDevice(webcam[comboBox1.SelectedIndex].MonikerString);
            cam.NewFrame += new NewFrameEventHandler(cam_NewFrame);
            cam.Start();
            
        }
        //Creats the image for picture box from the bitmap clone of the webcam source
        void cam_NewFrame(object sender, NewFrameEventArgs eventargs)
        {
            //Displays the current video source on the picturebox,
            //This creates a stream of video
            Bitmap bit = (Bitmap)eventargs.Frame.Clone();
            pictureBox2.Image = bit;

        }
        public void waitforint(string start)
        {
            printPhoto();
            
        }
        //Fires the take photo button, is called when the timer ticks
        public void printPhoto()
        {
            ///Problem lies here, not sure what I should do?
             
            pictureBox2.Focus();
            pictureBox2.Visible = true;
            pictureBox2.Image.Save(@"C:\picture\" + DateTime.Now.ToString("MM_dd_yyyy_hhmmss") + ".jpeg");
        }

        //Takes snapshot of license plate 3 seconds after scanner is initiated
        private void btnTakePhoto_Click(object sender, EventArgs e)
        {
            //Saves the current image of the picturebox to the students last name and date
            pictureBox2.Image.Save(@"C:\picture\" + current + DateTime.Now.ToString("MM_dd_yyyy_hhmmss") + ".jpeg");
            //Stop timer and wait for the next entry
            timer1.Stop();

        }
        public void do_Nothing()
        {


        }

    } }
    

