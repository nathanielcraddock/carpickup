﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPickupLine
{
    class CurrentCar
    {
        public string _Car;

        public string Car
        {
            get
            {
                return _Car;
            }
            set
            {
                _Car = value;
            }
        }
     
    }
}
