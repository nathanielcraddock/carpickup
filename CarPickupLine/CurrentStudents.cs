﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarPickupLine
{
    class CurrentStudents
    {
        public string StudentFName
        {
            get
            {
                return StudentFName;
            }
            set
            {
                StudentFName = value;
            }
        }
        public string StudentLName
        {
            get
            {
                return StudentLName;
            }
            set
            {
                StudentLName = value;
            }
        }
        public string CarMake
        {
            get
            {
                return CarMake;
            }
            set
            {
                CarMake = value;
            }
        }
        public string CarModel
        {
            get
            {
                return CarModel;
            }
            set
            {
                CarModel = value;
            }
        }
    }
}
