﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Video;
using AForge.Video.DirectShow;

namespace CarPickupLine
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //Get the barcode scanners input
        private FilterInfoCollection webcam;
        private VideoCaptureDevice cam;
        CurrentCar car = new CurrentCar();
        private void tbStudentID_TextChanged(object sender, EventArgs e)
        {

            //Declare string to hold the barcode scanners input
            string sid = "";
            //Wait for the barcode scanner to enter the entire studentnumber
            if (tbStudentID.TextLength < 12)
                for (int i = 0; i == tbStudentID.TextLength; i++)
                {
                    sid = tbStudentID.Text;
                }
            //Once the barcode scanner is done entering input call on the search method
            else {
                car.Car = tbStudentID.Text;
                search(car.Car);
            }
        }

        //Searches the database for the StudentNumber
        public void search(string sid)
        {
            try
            {
                // Creates a new db instance from the dbl I created using the three tables
                using (StudentsDataContext db = new StudentsDataContext())
                {
                    //Searches the database and displays students first name
                    string fName = (from row in db.Students
                                    where row.StudentNumber == sid
                                    select row.FName).First();
                    //Display results
                    label2.Text = fName;

                    //Searches the database and displays students last name
                    string lName = (from row in db.Students
                                    where row.StudentNumber == sid
                                    select row.LName).First();
                    //Display results
                    label3.Text = lName;
                    //Searches the database and displays the make of the students car 
                    string make = (from stud in db.Students
                                   join l in db.Lines on stud.StudentID equals l.StudentID into studGroup
                                   from l2 in studGroup
                                   join car in db.Cars on l2.CarId equals car.CarID
                                   where stud.StudentNumber == sid
                                   select car.Make).First();
                    //Display results
                    label4.Text = make;

                    //Searches the database and displays the model of the students car name
                    string model = (from stud in db.Students
                                    join l in db.Lines on stud.StudentID equals l.StudentID into studGroup
                                    from l2 in studGroup
                                    join car in db.Cars on l2.CarId equals car.CarID
                                    where stud.StudentNumber == sid
                                    select car.Model).First();
                    //Display results
                    label5.Text = model;
                }
            }
            catch
            {
                //If the student number is not found it asks for a manually entry
                //For some reason it wont let me scan after this
                MessageBox.Show("No User Found Please Manually Enter StudentNumber");

            }

            //Clear textbox on successful try and start the timer for photo
            tbStudentID.Text = "";
            CamLoad frm = new CamLoad();
            frm.current = car.Car;


        }

        private void Form1_Load(object sender, EventArgs e)
        {
            webcam = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            foreach (FilterInfo VideoCaptureDevice in webcam)
            {
                //Adds computer webcam to the combobox to allow selection
                comboBox1.Items.Add(VideoCaptureDevice.Name);
            }
            comboBox1.SelectedIndex = 0;
            //Selects the currently selected webcam source and creates a new instance of cam from it
            cam = new VideoCaptureDevice(webcam[comboBox1.SelectedIndex].MonikerString);
            cam.NewFrame += new NewFrameEventHandler(cam_NewFrame);
            cam.Start();
            CamLoad CamLoad = new CamLoad();
           CamLoad.Show();
        }
        void cam_NewFrame(object sender, NewFrameEventArgs eventargs)
        {
            //Displays the current video source on the picturebox,
            //This creates a stream of video
            Bitmap bit = (Bitmap)eventargs.Frame.Clone();
            pictureBox1.Image = bit;

        }

        private void btnLoadWebCam_Click(object sender, EventArgs e)
        {
            pictureBox1.Image.Save(@"C:\picture\" + DateTime.Now.ToString("MM_dd_yyyy_hhmmss") + ".jpeg");
        }
    }
    }


    

